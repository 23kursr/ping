import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class PlayerPaddle {

	int x;// x ,y tenhleg
	int y;
	int width = 15;
	int height = 40;
	int speed = 3;//PlayerPaddle-iin Bosoo tenhleg buyu Y tenhleg deer hodloh hurd
	
	Rectangle boundingBox;//boundingBox buyu collision hiih
	
	boolean goingUp = false;
	boolean goingDown = false;
	
	public PlayerPaddle(int x, int y)
	{
		this.x = x;
		this.y = y;
		
		boundingBox = new Rectangle(x , y , width , height);//ingesneer boundigbox buyu collisionii bairlal togloom ehlehed PPaddletai adil baina
		boundingBox.setBounds(x , y , width , height);
	}
	
	public void tick(Game game)//Paddle-d hodolgoon bolon Biyt hairtsagiig nemj ogno
	{
		boundingBox.setBounds(x,y,width,height);
		if(goingUp && y > 0)//tenhelgiin hurdiig nemj hasaj ogsonoor bairandaa hodolgoongui zogsohoos sergiilne
		{
			y = y - speed;
		}
		if(goingDown && y < game.getHeight() - height) // togloomiin windowoos taglaj bga hesgiin ondoriig avan niit ondoroos hassanaar buten hodloh bolomjtoi
		{
			y+=speed;
		}
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.GRAY);
		g.fillRect(x, y, width, height);
	}
}