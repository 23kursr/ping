import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class AIPaddle 
{

	/*
	 * AIPaddle-iin hevtee bolon bosoo tenhleg ondor urt orgon hurdiig zarlan onooj ogson
	 */
	int x;
	int y;
	int width = 15;
	int height = 40;
	int speed = 1;
	
	Rectangle boundingBox;//boundingBox buyu collision hiih
	

	
	public AIPaddle(int x, int y)
	{
		this.x = x;
		this.y = y;

		/*
		 * boundingBox hiisneer bombog hanad bish paddle deer oino
		 * this.x,this.y,this.size,this.size ni togloom ehlehed paddletai adilhan bairlald ehelne
		 */
		
		boundingBox = new Rectangle(x , y , width , height);
		boundingBox.setBounds(x , y , width , height);
	}
	
	public void tick(Game game)
	{
		boundingBox.setBounds(x , y , width , height);
		
		/*
		 * AI heseg 
		 */
		
		if(Game.ball.y < y + height && y >= 0)
		{
			y = y - speed;
		}
		else if(Game.ball.y > y && y + height <= game.getHeight())
		{
			y+= speed;
		}
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.BLUE);
		g.fillRect(x, y, width, height);
	}
}