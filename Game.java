import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
public class Game extends Canvas implements Runnable 
/*	Canvas ni JPanel gehdee arai iluu tuvshind
 * 	Runnable ni Codiig Process bolgono
 */
{
	private static final long serialVersionUID = 1L;
	
	/*
	 * Togtmoloor todorhoilogdson huvisagch object uusgelguigeer handah bolomjtoi
	 */
	
	public static PlayerPaddle player; //PlayerPaddle buyu toglogchiin tsohiuriig duudaj baina
	public static AIPaddle ai; //AIPaddle buyu AI iin tsohiuriig duudah
	public static Ball ball; //Bombogiig duudah
	InputHandler IH;//InputHandler - (Command huleen avah) funkts duudah
	
	JFrame frame; //Huree
	
	public final int WIDTH = 400; // orgon , 	
	public final int HEIGHT = WIDTH / 16 * 9; //ondor bolon windowiin haritsaa
	public final Dimension gameSize = new Dimension(WIDTH , HEIGHT); // 1 variable dotor ondor bolon orgong hiij ogoh
	public final String TITLE = "Ping Pong 70s";//Garchig
	/*
	 * final ni huvisagchiin utgiig togtmol bolgono
	 */
	BufferedImage image = new BufferedImage(WIDTH, HEIGHT , BufferedImage.TYPE_INT_RGB);
	//frame bolgoj zadlan image uusged tuundeere tgloomoo zurah
	
	static boolean gameRunning = false; //togloom odoogoor ajillahgui bga 
	
	int p1Score, p2Score;//Toglogchdiin onoo (score)
	
	public void run()//canvas ashiglahad zailshgui hereg boloh funkts
	{
		while(gameRunning) //togloom ehlehed ajillah zogsoltgui davtalt 
		{
			//davtaltand tick bolon render duudsanaar fps bolon togloomiig ajilluulj ogno
			tick();
			render();
		
			try //togloom frame burt 7 millsecondiing zogsolttoi
			{
				Thread.sleep(7);
			}
			catch(Exception e)//Aldaa hynah funkts
			{
				e.printStackTrace();
			}
		}
	}
	public synchronized void start() //togloom ehluuleh 
	{
		gameRunning = true;
		new Thread(this).start();
	}
	public static synchronized void stop() //togloom zogsooh funkts
	{
		gameRunning = false;
		System.exit(0);
	}
	
	public Game() // Baiguulagch funkts
	{
		frame = new JFrame();//Togloomiin huree baiguulah
		
		/*
		 * togloomiin tsonhnii sizeiig fixed bolgoh
		 */
		setMinimumSize(gameSize);
		setPreferredSize(gameSize);
		setMaximumSize(gameSize);
		
		frame.add(this , BorderLayout.CENTER);
		frame.pack(); //bugdiin JFrame dotor bairluulan bagtaah 
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// garah tovchiig darsnii daraa buh processiig zogsooj programiig untraah
		frame.setVisible(true);
		frame.setResizable(false);//hemjeeg bagasgaj ihesgeh tohirgoog false bolgov
		frame.setTitle(TITLE);
		frame.setLocationRelativeTo(null);//tsonhiig delgetsnii gold bairluulah
		
		IH = new InputHandler(this);
		
		player = new PlayerPaddle(10,60);//playerPaddle - 
		ai = new AIPaddle(getWidth() -20 , 60);
		ball = new Ball(getWidth() / 2 , getHeight() / 2);
	} // baiguulagch funktsiin togsgol
	public void tick() // updates - shinechleluud (Frames)
	{
		player.tick(this);
		ai.tick(this);
		ball.tick(this);
	}
	public void render()//
	{
		BufferStrategy bs = getBufferStrategy(); // zavsriin hadgalah 
		if(bs == null)//hervee buffer baihgui bol shineer argumentiin utga ni 3 tai tentsuu buffer uusgene tegseneer reduce the tearing 
		{
			createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();//graphiciig ashiglan bufferdaa onoosnoor image deer zurag dargdana
		
		g.drawImage(image, 0 , 0 , getWidth() , getHeight() , null); //ariin talbai
		
		g.setColor(Color.WHITE); //score ongo
		
		//score ++1
		g.drawString("1-p Toglogch: " + p1Score, 5, 10);
		g.drawString("Hiimel oyun: " + p2Score, getWidth() -90 , 10);
		
		/*
		 * render ni graphiciig zurna
		 */
		
		player.render(g);
		ai.render(g);
		ball.render(g);
		
		g.dispose();
		bs.show();//bufferiig haruulna
	}
}