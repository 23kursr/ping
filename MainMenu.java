import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
public class MainMenu extends JFrame         
/*
 * JFrame ees udamshij  bui   MainMenu
 */
{
	private static final long serialVersionUID = 1L;  
	/*
	*Togtmoloor todorhoilogdsn huwisagch,,object uusgelgugeer handh
	*/
	int screenWidth = 250;
	int screenHeight = 250;
	int buttonWidth = 100;        
	int buttonHeight = 40;    
	JButton Play, Quit; 								
	
	public MainMenu()
	{		
		addButtons();//Tovch nemeh
		addActions();//Uildel nemeh
		getContentPane().setLayout(null);//golluulah
	    Play.setBounds((screenWidth - buttonWidth) / 2 , 5 , buttonWidth, buttonHeight); // Play tovchiig bairshuulah
	    Quit.setBounds((screenWidth - buttonWidth) / 2 , 50 , buttonWidth , buttonHeight); //Quit tovchig bairluulah
		
	    getContentPane().add(Play); 
		getContentPane().add(Quit);  //JFrame luu tovch nemeh 
		/*
		 * JFrame uud
		 */
		pack();                 
		setVisible(true);
		setLocationRelativeTo(null);
		setSize(screenWidth, screenHeight);
		setTitle("Pong Menu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}
	
	private void addButtons()   
	/* 
	 * huwiin handalttai addButtons iin baiguulagch function 
	 */
	{
		Play = new JButton("Play");   
		Quit = new JButton("Quit");     	
	}
	private void addActions()    
	/* 
	 * huwiin handalttai addButtons iin baiguulagch function 
	 */
	{
		Play.addActionListener(new ActionListener() 
				/*
				 * Play tovchiig huleen avch shineer actionlistener gargah
				 */
		{
			public void actionPerformed(ActionEvent e)   
			/*
			 * togloomiig duudaj ajilluulah
			 */
			{
				Game game = new Game();                   //Game-iig shineer ehluulne(reset)
				game.start();                         	  //Togloomiig ehluuleh function
			}
		});       
		Quit.addActionListener(new ActionListener() 
              	/*
				 * quit tovchiig huleen avch shineer actionlistener gargan avah
				 */
			{
			public void actionPerformed(ActionEvent e)                  
			{
				System.exit(0); //*programaas garah
			}
		});     //Quit tovch
	}
}